Welcome!    
If you got here, it means you want to learn how to contribute to my little project. That's great! Thank you for your interest.    

## Code ##
This may be a bit painful, the quality of code can be very... Questionable, to say the least. However, if you like adventures, you are free to contribute to the project! All I ask you is just so you send merge requests to the testing unstable.
Current branch rules are as follows:     
`master` - the most stable branch that has been tested for at least one day.    
`testing `- testing branch is what all of my hosted scripts run. It is used to test the code before merging it with master branch. All languages must be finished for code to be pushed to this branch.    
`unstable` - the most unstable of unstable. Can result in frying the machine, well, not really, but all of new changes come here first, after some internal testing and translations it can be merged with testing branch.    

## Translations ##
If you speak in other languages than English, you are more than welcome to. All you need to do, is grab the [translation template file](rcgcdw.pot) and fill out the empty strings under every English sentence. You can use software like Poedit or use your favorite text editor to do that. If you are looking for examples, you can look at [Polish translation](/locale/pl/LC_MESSAGES/rcgcdw.po). After you translate it, you can either send a merge request to the testing branch, or, if you don't know git [contact me directly](https://minecraft.gamepedia.com/User:Frisk#Contact) so I can do this for you.
